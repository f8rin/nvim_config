set mouse=a 
set encoding=utf-8
set number
set relativenumber
set noswapfile
set scrolloff=7

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set fileformat=unix
filetype indent on      " load filetype-specific indent files

" for tabulation
set smartindent
set tabstop=2
set expandtab
set shiftwidth=2

inoremap jk <esc>

call plug#begin('~/.vim/plugged')

Plug 'preservim/nerdcommenter'
Plug 'nvim-lua/plenary.nvim'

Plug 'miyakogi/seiya.vim'
Plug 'scrooloose/nerdtree'
Plug 'ap/vim-css-color'

Plug 'flazz/vim-colorschemes'
source ~/.vim/plugged/vim-colorschemes/colors/PaperColor.vim

Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='base16_gruvbox_dark_medium'
Plug 'nvie/vim-flake8'
Plug 'vim-airline/vim-airline'
Plug 'ryanoasis/vim-devicons'
Plug 'tc50cal/vim-terminal'
Plug 'terryma/vim-multiple-cursors'
Plug 'preservim/tagbar'

call plug#end()

let mapleader = ","

" Netrw file explorer settings
let g:netrw_banner = 0 " hide banner above files
let g:netrw_liststyle = 3 " tree instead of plain view
let g:netrw_browse_split = 3 " vertical split window when Enter pressed on file

" Automatically format frontend files with prettier after file save
let g:prettier#autoformat = 1
let g:prettier#autoformat_require_pragma = 0

" Disable quickfix window for prettier
let g:prettier#quickfix_enabled = 0

" Turn on vim-sneak
let g:sneak#label = 1

" turn off search highlight
nnoremap ,<space> :nohlsearch<CR>
